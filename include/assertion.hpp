#ifndef ASSERTION_H
#define ASSERTION_H

#include "formula/language.hpp"

class Assertion
{
	public :
		static std::vector<Model> get_stable_models(Formula* f);
		static void print_stable_models(Formula* f);

		static std::vector<Model> get_all_models_that_satisfy(Formula* f);
		static void print_all_models_that_satisfy(Formula* f);

		static bool satisfies(Model HT, Formula* f);

		static bool HT_implies(Formula* theory, Formula* f);
		static void print_HT_implies(Formula* theory, Formula* f);

		static bool EL_implies(Formula* theory, Formula* f);
		static void print_EL_implies(Formula* theory, Formula* f);
	protected : 
		Assertion();
};

#endif