#ifndef VALUATION_H
#define VALUATION_H

#include <unordered_set>
#include <vector>

class Valuation
{
	public :
		Valuation();
		Valuation(std::vector<std::string> atoms);
		void add_atom(std::string atom);
		void rm_atom(std::string atom);
		bool contains(std::string atom);
		bool is_included_in(Valuation T);
		bool is_strictly_included_in(Valuation T);
		bool equals(Valuation V);

		virtual void print();
	protected : 
		std::unordered_set<std::string> atoms;
};

#endif