#ifndef MODEL_H
#define MODEL_H

#include <vector>

#include "valuation.hpp"

class Model
{
	public :
		Model();
		Model(Valuation H, Valuation T);
		void set_H(Valuation VH);
		void set_T(Valuation VT);
		Valuation H();
		Valuation T();
		static std::vector<Model> all_models_from_atoms(std::vector<std::string> atoms);
		virtual void print();
		bool equals(Model HT);
		bool is_total();

	protected : 
		Valuation m_H;
		Valuation m_T;
		void check_inclusion();
};

#endif