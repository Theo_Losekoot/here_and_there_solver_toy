#ifndef BIB_H
#define BIB_H

#include <vector>
#include <cmath>
#include <algorithm>
#include <string>
#include <iostream>


template <class T>
bool is_in_vector(std::vector<T> v, T e)
{
	return (std::find(v.begin(), v.end(), e) != v.end());
}

template <class T>
std::vector<std::vector<T>> all_subsets(std::vector<T> elements)
{
	std::vector<std::vector<T>> all_subsets;
	unsigned int count = pow(2, elements.size());
	// The outer for loop will run 2^n times to print all subset .
	// Here variable i will act as a binary counter
	for (unsigned int i = 0; i < count; i++)
	{
		std::vector<T> subset;
		// The inner for loop will run n times , As the maximum number of elements a set can have is n
		// This loop will generate a subset
		for (unsigned int j = 0; j < elements.size(); j++)
		{
			// This if condition will check if jth bit in binary representation of  i  is set or not
			// if the value of (i & (1 << j)) is greater than 0 , include arr[j] in the current subset
			// otherwise exclude arr[j]
			if ((i & (1 << j)) > 0)
				subset.push_back(elements.at(j));
		}
		all_subsets.push_back(subset);
	}
	return all_subsets;
}


template <class T>
std::vector<T> vector_union(std::vector<T> v1, std::vector<T> v2)
{
	for(T e : v2)
	{
		if(!is_in_vector(v1, e))
			v1.push_back(e);
	}
	return v1;
}

template <class T>
void print_vector(std::vector<T> v, std::string separator)
{
	for(unsigned int i = 0; i<v.size()-1; i++)
	{
		T e = v.at(i);
		e.print();
		std::cout << separator;
	}
	if(v.size() >= 1)
		v.back().print();
}




#endif