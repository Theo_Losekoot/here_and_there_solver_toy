#ifndef ATOM_H
#define ATOM_H

#include "formula.hpp"

class Atom : public Formula
{
	public :
		Atom(std::string atom);
		virtual bool value(Model HT);
		virtual void print();
		virtual Formula* copy();
		virtual std::vector<std::string> alphabet();
	protected : 
		std::string m_atom;
};

#endif