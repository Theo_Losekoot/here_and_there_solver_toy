#ifndef FORMULA_H
#define FORMULA_H

#include "../model/model.hpp"

#include <iostream>

class Formula
{
	public :
		virtual bool value(Model HT) = 0;
		virtual ~Formula();
		virtual void print() = 0;
		virtual Formula* copy() = 0;
		virtual std::vector<std::string> alphabet() = 0;
	protected : 

};

#endif