#ifndef AND_H
#define AND_H

#include "formula.hpp"

#include <iostream>

class And : public Formula
{
	public :
		And(Formula* f1, Formula* f2);
		virtual ~And();
		virtual bool value(Model HT);
		virtual void print();
		virtual Formula* copy();
		virtual std::vector<std::string> alphabet();
	protected : 
		Formula* m_f1;
		Formula* m_f2;

};

#endif