#ifndef BOT_H
#define BOT_H

#include "formula.hpp"

class Bot : public Formula
{
	public :
		Bot();
		Bot(Bot* b);
		virtual bool value(Model HT);
		virtual void print();
		virtual Formula* copy();
		virtual std::vector<std::string> alphabet();
	protected : 

};

#endif