#ifndef BIIMPLIES_H
#define BIIMPLIES_H

#include "formula.hpp"
#include "implies.hpp"

class Biimplies : public Formula
{
	public :
		Biimplies(Formula* f1, Formula* f2);
		virtual ~Biimplies();
		virtual bool value(Model HT);
		virtual void print();
		virtual Formula* copy();
		virtual std::vector<std::string> alphabet();
	protected : 
		Formula* m_f1;
		Formula* m_f2;
};

#endif