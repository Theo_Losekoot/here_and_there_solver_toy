#ifndef NOT_H
#define NOT_H

#include "formula.hpp"
#include "implies.hpp"
#include "bot.hpp"

class Not : public Formula
{
	public :
		Not(Formula* f);
		virtual ~Not();
		virtual bool value(Model HT);
		virtual void print();
		virtual Formula* copy();
		virtual std::vector<std::string> alphabet();

	protected : 
		Formula* m_f;
};

#endif