#ifndef LANGUAGE_H
#define LANGUAGE_H

#include "formula.hpp"
#include "bot.hpp"
#include "and.hpp"
#include "or.hpp"
#include "implies.hpp"
#include "biimplies.hpp"
#include "not.hpp"
#include "atom.hpp"
#include "../model/model.hpp"
#include "../model/valuation.hpp"

#endif