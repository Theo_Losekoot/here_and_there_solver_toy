#ifndef IMPLIES_H
#define IMPLIES_H

#include "formula.hpp"

class Implies : public Formula
{
	public :
		Implies(Formula* f1, Formula* f2);
		virtual ~Implies();
		virtual bool value(Model HT);
		virtual void print();
		virtual Formula* copy();
		virtual std::vector<std::string> alphabet();

	protected : 
		Formula* m_f1;
		Formula* m_f2;
};

#endif