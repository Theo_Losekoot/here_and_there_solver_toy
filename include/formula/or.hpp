#ifndef OR_H
#define OR_H

#include "formula.hpp"

class Or : public Formula
{
	public :
		Or(Formula* f1, Formula* f2);
		Or(Or* f1);
		virtual ~Or();
		virtual bool value(Model HT);
		virtual void print();
		virtual Formula* copy();
		virtual std::vector<std::string> alphabet();

	protected : 
		Formula* m_f1;
		Formula* m_f2;
};

#endif