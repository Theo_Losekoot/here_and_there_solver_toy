#ifndef FORMULA_MAKER_H
#define FORMULA_MAKER_H

#include <vector>

#include "formula/language.hpp"

class FormulaMaker
{
	public : 
		static Formula* conjunction(std::vector<Formula*> formulas);
	
	protected : 
		FormulaMaker();

};



#endif