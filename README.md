#  HT (Here-and-There) logic solver #

This project is a simplified HT solver.
It exists because I tried to understand this logic and computing everything by hand was too long.

To run, do:
`make` then `./exec`

To give a formula in input, you have to modify the `main.cpp` file and re-compile. 






