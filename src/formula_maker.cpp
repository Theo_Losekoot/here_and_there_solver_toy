#include "formula_maker.hpp"

Formula* FormulaMaker::conjunction(std::vector<Formula*> formulas)
{
	if(formulas.size()==0)
	{
		return new Not(new Bot());
	}
	else if (formulas.size() == 1) 
	{
		Formula* f = formulas.at(0);
		return new And(f->copy(), f);
	}
	else if (formulas.size() == 2)
	{
		return new And(formulas.at(0), formulas.at(1));
	}
	else
	{
		Formula* last = formulas.back(); formulas.pop_back();
		return new And(last, conjunction(formulas));
	}
}

FormulaMaker::FormulaMaker()
{
}
