#include "formula/atom.hpp"

#include <iostream>

using namespace std;

Atom::Atom(string atom)
{
	m_atom = atom;
}

bool Atom::value(Model HT)
{
	return HT.H().contains(m_atom);
}


void Atom::print()
{	
	std::cout << m_atom;
}


Formula* Atom::copy()
{
	return new Atom(m_atom);
}


vector<string> Atom::alphabet()
{
	vector<string> alphabet;
	alphabet.push_back(m_atom);
	return alphabet;
}