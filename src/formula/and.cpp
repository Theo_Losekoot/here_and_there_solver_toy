#include "formula/and.hpp"

#include <iostream>

#include "bib.hpp"

using namespace std;

And::And(Formula* f1, Formula* f2)
{
	m_f1 = f1;
	m_f2 = f2;
}

And::~And()
{
	delete m_f1;
	delete m_f2;
}

bool And::value(Model HT)
{
	return m_f1->value(HT) && m_f2->value(HT);
}

void And::print()
{	
	cout << "(";
	m_f1->print();
	cout << " ^ ";
	m_f2->print();
	cout << ")";
}

Formula* And::copy()
{
	return new And(m_f1->copy(), m_f2->copy());
}


vector<string> And::alphabet()
{
	return vector_union(m_f1->alphabet(), m_f2-> alphabet());
}