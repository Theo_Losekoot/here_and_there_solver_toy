#include "formula/not.hpp"

#include <iostream>

using namespace std;

Not::Not(Formula* f)
{
	m_f = f;
}

Not::~Not()
{
	delete m_f;
}

bool Not::value(Model HT)
{
	
	return Implies(m_f->copy(), new Bot()).value(HT);
}


void Not::print()
{	
	cout << "¬(";
 	m_f->print();
	cout << ")";
}


Formula* Not::copy()
{
	return new Not(m_f->copy());
}

vector<string> Not::alphabet()
{
	return m_f->alphabet();
}