#include "formula/biimplies.hpp"

#include <iostream>

#include "bib.hpp"


using namespace std;

Biimplies::Biimplies(Formula* f1, Formula* f2)
{
	m_f1 = f1;
	m_f2 = f2;
}

Biimplies::~Biimplies()
{
	delete m_f1;
	delete m_f2;
}

bool Biimplies::value(Model HT)
{
	bool one_way = Implies(m_f1->copy(), m_f2->copy()).value(HT);
	bool the_other = Implies(m_f2->copy(), m_f1->copy()).value(HT);

	return one_way && the_other;
}


void Biimplies::print()
{	
	std::cout << "(";
	m_f1->print();
	std::cout << " <-> ";
	m_f2->print();
	std::cout << ")";
}


Formula* Biimplies::copy()
{
	return new Biimplies(m_f1->copy(), m_f2->copy());
}

vector<string> Biimplies::alphabet()
{
	return vector_union(m_f1->alphabet(), m_f2-> alphabet());
}