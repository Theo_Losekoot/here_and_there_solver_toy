#include "formula/implies.hpp"

#include <iostream>

#include "bib.hpp"


using namespace std;

Implies::Implies(Formula* f1, Formula* f2)
{
	m_f1 = f1;
	m_f2 = f2;
}

Implies::~Implies()
{
	delete m_f1;
	delete m_f2;
}

bool Implies::value(Model HT)
{
	bool HT_satisfies =  (m_f2->value(HT) || (!m_f1->value(HT)));
	bool TT_satisfies =  (m_f2->value(Model(HT.T(), HT.T())) || (!m_f1->value(Model(HT.T(), HT.T()))));
	
	return HT_satisfies && TT_satisfies;
}


void Implies::print()
{	
	std::cout << "(";
	m_f1->print();
	std::cout << " -> ";
	m_f2->print();
	std::cout << ")";
}


Formula* Implies::copy()
{
	return new Implies(m_f1->copy(), m_f2->copy());
}

vector<string> Implies::alphabet()
{
	return vector_union(m_f1->alphabet(), m_f2-> alphabet());
}




