#include "formula/or.hpp"

#include <iostream>

#include "bib.hpp"


using namespace std;

Or::Or(Formula* f1, Formula* f2)
{
	m_f1 = f1;
	m_f2 = f2;
}

Or::~Or()
{
	delete m_f1;
	delete m_f2;
}

bool Or::value(Model HT)
{
	return m_f1->value(HT) || m_f2->value(HT);
}


void Or::print()
{	
	std::cout << "(";
	m_f1->print();
	std::cout << " v ";
	m_f2->print();
	std::cout << ")";
}

Formula* Or::copy()
{
	return new Or(m_f1->copy(), m_f2->copy());
}

vector<string> Or::alphabet()
{
	return vector_union(m_f1->alphabet(), m_f2-> alphabet());
}