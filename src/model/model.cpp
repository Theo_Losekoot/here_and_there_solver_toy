#include "model/model.hpp"

#include <iostream>
#include <vector>
#include <stdexcept>


#include "bib.hpp"


using namespace std;

Model::Model()
{
	
}

Model::Model(Valuation H, Valuation T)
{
	m_H = H;
	m_T = T;
	check_inclusion();
}

void Model::check_inclusion()
{
	if(!m_H.is_included_in(m_T))
		throw std::invalid_argument("H should be included in T");
}

void Model::set_H(Valuation H)
{
	m_H = H;
	check_inclusion();
}

void Model::set_T(Valuation T)
{
	m_T = T;
	check_inclusion();
}

Valuation Model::H()
{
	return m_H;
}

Valuation Model::T()
{
	return m_T;
}


vector<Model> Model::all_models_from_atoms(vector<string> atoms)
{
	vector<Model> models;
	vector<vector<string>> all_subsets_atoms = all_subsets(atoms);
	for(vector<string> vect_T : all_subsets_atoms)
	{
		Valuation T = Valuation(vect_T);
		for(vector<string> vect_H : all_subsets_atoms)
		{
			Valuation H = Valuation(vect_H);
			if(H.is_included_in(T))
			{
				models.push_back(Model(H, T));
			}
		}
	}	
	return models;
}


void Model::print()
{	
	cout << "<";
	m_H.print();
	cout << ", ";
	m_T.print();
	cout << ">";	
}

bool Model::equals(Model HT)
{
	return HT.H().equals(m_H) && HT.T().equals(m_T);
}

bool Model::is_total()
{
	return m_H.equals(m_T);
}



