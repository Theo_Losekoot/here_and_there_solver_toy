#include "model/valuation.hpp"

#include <iostream>
#include <vector>

using namespace std;

Valuation::Valuation()
{
	
}

Valuation::Valuation(std::vector<std::string> vect_atoms)
{
	for(string atom : vect_atoms)
	{
		atoms.insert(atom);
	}
}


void Valuation::add_atom(string atom)
{
	this->atoms.insert(atom);
}

void Valuation::rm_atom(string atom)
{
	this->atoms.erase(atom);
}

bool Valuation::contains(string atom)
{
	return !(atoms.find(atom) == atoms.end());
}

bool Valuation::is_included_in(Valuation T)
{
	for(string atom : atoms)
	{
		if(!T.contains(atom))
			return false;
	}
	return true;
}

bool Valuation::is_strictly_included_in(Valuation T)
{
	return this->is_included_in(T) && (! this->equals(T));
}


bool Valuation::equals(Valuation V)
{
	return V.is_included_in(*this) && this->is_included_in(V);
}


void Valuation::print()
{	
	cout << "{";

	vector<string> vect_atoms;
	for(string atom : atoms)
	{	
		vect_atoms.push_back(atom);
	}

	for(unsigned int i=0; i+1<vect_atoms.size(); i++)
	{
		string atom = vect_atoms.at(i);
		cout << atom << ", ";
	}
	if(vect_atoms.size() > 0)
	{
		cout << vect_atoms.back();
	}

	cout << "}";
}


