#include "assertion.hpp"

#include <iostream>
#include <algorithm>    // std::sort
#include <vector>  
#include <cmath>


#include "bib.hpp"

using namespace std;


vector<Model> Assertion::get_stable_models(Formula* f)
{
	vector<Model> satisfying_models = Assertion::get_all_models_that_satisfy(f);
	vector<Model> stable_models;

	for(Model TT : satisfying_models)
	{
		if(TT.is_total())
		{
			bool is_stable = true;
			for(Model HT : satisfying_models)
			{
				
				if(HT.T().equals(TT.T()) && HT.H().is_strictly_included_in(TT.H()))
					is_stable=false;
			}
			if(is_stable)
				stable_models.push_back(TT);
		}
	}
	return stable_models;
}

void Assertion::print_stable_models(Formula* f)
{
	vector<Model> stable_models = Assertion::get_stable_models(f);
	cout << "Formula "; 
	f->print(); 
	cout << " is EL-";
	if(stable_models.size() > 0)
	{
		cout << "SAT!" << endl;
		cout << "its stable models are : " << endl;
		for(Model v : stable_models)
		{
			v.print();
			cout << endl;
		}
		cout << endl;
	}
	else
		cout << "UNSAT!" << endl;

}



std::vector<Model> Assertion::get_all_models_that_satisfy(Formula* f)
{
	vector<Model> all_possible_models = Model::all_models_from_atoms(f->alphabet());
	vector<Model> all_satisfying_models;
	for(Model HT : all_possible_models)
	{
		if(Assertion::satisfies(HT, f))
		{
			all_satisfying_models.push_back(HT);
		}
	}
	return all_satisfying_models;
}


void Assertion::print_all_models_that_satisfy(Formula* f)
{
	vector<Model> models = Assertion::get_all_models_that_satisfy(f);
	cout << "Formula "; 
	f->print(); 
	cout << " is HT-";
	if(models.size() > 0)
	{
		cout << "SAT!" << endl;
	
		cout << "its models are : " << endl;
		for(Model v : models)
		{
			v.print();
			cout << endl;
		}
	}
	else
		cout << "UNSAT!" << endl;

	cout << endl;
}



bool Assertion::satisfies(Model HT, Formula* f)
{
	return f->value(HT);
}


bool Assertion::HT_implies(Formula* theory, Formula* f)
{
	//vector<Model> all_possible_models = Model::all_models_from_atoms(f->alphabet());
	vector<Model> models_theory = Assertion::get_all_models_that_satisfy(theory);
	for(Model HT : models_theory)
	{
		if(!Assertion::satisfies(HT, f))
		{
			return false;
		}
	}	
	return true;	
}

void Assertion::print_HT_implies(Formula* theory, Formula* f)
{
	theory->print();
	cout << " HT-implies " ;
	f->print();
	cout << " : " << std::boolalpha << Assertion::HT_implies(theory, f) << std::noboolalpha << endl;
}


bool Assertion::EL_implies(Formula* theory, Formula* f)
{
	//vector<Model> all_possible_models = Model::all_models_from_atoms(f->alphabet());
	vector<Model> models_theory = Assertion::get_stable_models(theory);
	for(Model HT : models_theory)
	{
		if(!Assertion::satisfies(HT, f))
		{
			return false;
		}
	}	
	return true;	
}

void Assertion::print_EL_implies(Formula* theory, Formula* f)
{
	theory->print();
	cout << " EL-implies " ;
	f->print();
	cout << " : " << std::boolalpha << Assertion::EL_implies(theory, f) << std::noboolalpha << endl;
}





