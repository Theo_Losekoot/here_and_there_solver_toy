#include <iostream>

#include "formula_maker.hpp"
#include "assertion.hpp"

using namespace std;

int main(int argc, char** argv)
{


	if(false)
	{
		cout << endl << "---------------------------------------" << endl;	
		cout << "HT models example" << endl << endl;

		Formula* f = new Or(new Atom("p"), new Atom("q"));
		Formula* lit = new Not(new Not(new Atom("p")));
	
		std::vector<Formula*> formulas;
		formulas.push_back(f);
		formulas.push_back(lit);
		Formula* program = FormulaMaker::conjunction(formulas);

		Assertion::print_all_models_that_satisfy(program);
	
		delete program;

	}
	if(false)
	{
		cout << endl << endl <<  "---------------------------------------" << endl;	
		cout << "Stable models example" << endl << endl;

		Formula* f = new Or(new Atom("p"), new Atom("q"));
		Formula* lit = new Not(new Not(new Atom("p")));
	
		std::vector<Formula*> formulas;
		formulas.push_back(f);
		formulas.push_back(lit);
		Formula* program = FormulaMaker::conjunction(formulas);

		Assertion::print_stable_models(program);

		delete program;
	}


	if(true)
	{
		cout << endl << endl <<  "---------------------------------------" << endl;	
		cout << "Proof that EL (Equilibrium logic) is non-monotonic : " << endl << endl;

		Formula* theory = 	new Or
							(
								new Not(new Not(new Atom("p"))),
								new Atom("q")
							);
	
		Formula* augmented_theory = new And
									(
										theory,
										new Atom("p")
									);
										
		Formula* phi = new Atom("q");


		cout << "We have a theory that implies phi but an augmented theory that does not." << endl;
		Assertion::print_EL_implies(theory, phi);
		Assertion::print_EL_implies(augmented_theory, phi);
		cout << endl;


		cout << "STABLE MODELS : (Theory, then augmented_theory)" << endl;
		Assertion::print_stable_models(theory);
		Assertion::print_stable_models(augmented_theory);


		delete augmented_theory;
		delete phi;
	}


	return 0;
}